import axios from "axios";

const REST_API_BASE_URL = 'http://localhost:8080/api/v1/customers'

export const listCustomer = () => {
    return axios.get(REST_API_BASE_URL)
}

export const filterCustomer = (nama, alamat, kota) => {
    return axios.get(`${REST_API_BASE_URL}?nama=${nama}&alamat=${alamat}&kota=${kota}`)
}

export const getCustomer = (customerId) => {
    return axios.get(REST_API_BASE_URL + '/' + customerId)
}

export const addCustomer = (customer) => {
    return axios.post(REST_API_BASE_URL, customer)
}

export const updateCustomer = (customerId, customer) => {
    return axios.put(REST_API_BASE_URL + '/' + customerId, customer)
}

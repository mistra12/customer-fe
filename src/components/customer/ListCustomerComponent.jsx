import { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'
import { filterCustomer, listCustomer } from '../../services/CustomerServise'
import moment from 'moment'

const ListCustomerComponent = () => {

    const [customers, setCustomers] = useState([])
    const [nama, setNama] = useState('')
    const [alamat, setAlamat] = useState('')
    const [kota, setKota] = useState('')
    const navigator = useNavigate()

    useEffect(() => {
        getAllCustomer()
    }, [])

    function getAllCustomer() {
        listCustomer().then(response => {
            setCustomers(response.data)
        }).catch(error => {
            console.error(error);
        })
    }

    function addCustomer() {
        navigator('/add-customer')
    }

    function searchCustomer(e) {
        e.preventDefault()
        filterCustomer(nama, alamat, kota).then(response => {
            setCustomers(response.data)
        }).catch(error => {
            console.log(error);
        })
    }

    function updateCustomer(id) {
        navigator(`/edit-customers/${id}`)
    }

    return (
        <div className='container mt-4'>
            <h5>List of Customer</h5>
            <div className='mb-3'>
                <button className='btn btn-primary mb-2' onClick={addCustomer}>Add Cutomer</button>
            </div>
            <form className="d-flex" role="search">

                <input
                    className="form-control me-2"
                    type="search"
                    name='nama'
                    value={nama}
                    onChange={e => setNama(e.target.value)}
                    placeholder="Nama"
                    aria-label="Search" />
                <input
                    className="form-control me-2"
                    type="search"
                    name='alamat'
                    value={alamat}
                    onChange={e => setAlamat(e.target.value)}
                    placeholder="Alamat"
                    aria-label="Search" />
                <input
                    className="form-control me-2"
                    type="search"
                    name='kota'
                    value={kota}
                    onChange={e => setKota(e.target.value)}
                    placeholder="Kota"
                    aria-label="Search" />
                <button className="btn btn-outline-success" onClick={searchCustomer} >Cari</button>
            </form>
            <table className='table table-hover'>
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Nama</th>
                        <th>Alamat</th>
                        <th>Kota</th>
                        <th>Provinsi</th>
                        <th>Tanggal Registrasi</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {customers.map(data =>
                        <tr key={data.id}>
                            <td> {data.id} </td>
                            <td> {data.nama} </td>
                            <td> {data.alamat} </td>
                            <td> {data.kota} </td>
                            <td> {data.provinsi} </td>
                            <td> {moment(data.tglRegistrasi).format('MMMM Do YYYY, h:mm:ss a')} </td>
                            <td> {data.status} </td>
                            <td>
                                <button className='btn btn-info' onClick={() => updateCustomer(data.id)}> Update </button>
                            </td>
                        </tr>
                    )}
                </tbody>
            </table>
        </div>
    )
}

export default ListCustomerComponent
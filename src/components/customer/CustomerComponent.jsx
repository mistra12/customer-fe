import { useEffect, useState } from "react"
import { useNavigate, useParams } from "react-router-dom"
import { addCustomer, getCustomer, updateCustomer } from "../../services/CustomerServise"
import moment from "moment"

const CustomerComponent = () => {
    const [nama, setNama] = useState('')
    const [alamat, setAlamat] = useState('')
    const [kota, setKota] = useState('')
    const [provinsi, setProvinsi] = useState('')
    const [tglRegistrasi, setTanggalRegistrasi] = useState('')
    const [status, setStatus] = useState('')


    const { id } = useParams()

    const [errors, setErrors] = useState({
        nama: '',
        alamat: '',
        kota: '',
        provinsi: '',
        tglRegistrasi: '',
        status: ''
    })

    const navigator = useNavigate()

    useEffect(() => {

        if (id) {
            getCustomer(id).then((response) => {
                setNama(response.data.nama);
                setAlamat(response.data.alamat);
                setProvinsi(response.data.provinsi);
                setKota(response.data.kota);
                setTanggalRegistrasi(response.data.tglRegistrasi);
                setStatus(response.data.status);
            }).catch(error => {
                console.error(error);
            })
        }

    }, [id])

    function saveOrUpdateCustomer(e) {
        e.preventDefault()
        if (validateForm()) {
            const customer = { nama, alamat, kota, provinsi, tglRegistrasi, status }
            customer.tglRegistrasi = moment(customer.tglRegistrasi).local()

            if (id) {
                updateCustomer(id, customer).then(response => {
                    console.log(response);
                    navigator('/customers')
                }).catch(error => {
                    console.error(error);
                })
            } else {
                addCustomer(customer).then(response => {
                    console.log(response);
                    navigator("/customers")
                }).catch(errors => {
                    console.error(errors);
                })
            }

        }

    }

    function validateForm() {
        let valid = true
        const errorsCopy = { ...errors }

        if (nama.trim()) {
            errorsCopy.nama = ''
        } else {
            errorsCopy.nama = 'Nama wajib diisi'
            valid = false
        }

        if (alamat.trim()) {
            errorsCopy.alamat = ''
        } else {
            errorsCopy.alamat = 'Alamat wajib diisi'
            valid = false
        }

        if (kota.trim()) {
            errorsCopy.kota = ''
        } else {
            errorsCopy.kota = 'Kota wajib diisi'
            valid = false
        }

        setErrors(errorsCopy)

        return valid
    }

    function pageTitle() {
        if (id) {
            return <h2 className="text-center">Update Customer</h2>
        } else {
            return <h2 className="text-center">Add Customer</h2>
        }
    }


    return (
        <div className="container">
            <br /> <br />
            <div className="row">
                <div className="card col-md-6 offset-md-3 offset-md-3">
                    {pageTitle()}
                    <div className="card-body">
                        <form action="">
                            <div className="form-group mb-2">
                                <label className="form-label">Nama:</label>
                                <input
                                    type="text"
                                    placeholder="Nama"
                                    name="nama"
                                    value={nama}
                                    className={`form-control ${errors.nama ? 'is-invalid' : ''}`}
                                    onChange={e => setNama(e.target.value)} />
                                {errors.nama && <div className="invalid-feedback"> {errors.nama} </div>}
                            </div>
                            <div className="form-group mb-2">
                                <label className="form-label">Alamat:</label>
                                <input
                                    type="text"
                                    placeholder="Alamat"
                                    name="alamat"
                                    value={alamat}
                                    className={`form-control ${errors.alamat ? 'is-invalid' : ''}`}
                                    onChange={e => setAlamat(e.target.value)} />
                                {errors.alamat && <div className="invalid-feedback"> {errors.alamat} </div>}

                            </div>
                            <div className="form-group mb-2">
                                <label className="form-label">Kota:</label>
                                <input
                                    type="text"
                                    placeholder="Kota"
                                    name="kota"
                                    value={kota}
                                    className={`form-control ${errors.kota ? 'is-invalid' : ''}`}
                                    onChange={e => setKota(e.target.value)} />
                                {errors.kota && <div className="invalid-feedback"> {errors.kota} </div>}
                            </div>

                            <div className="form-group mb-2">
                                <label className="form-label">Provinsi:</label>
                                <input
                                    type="text"
                                    placeholder="Provinsi"
                                    name="provinsi"
                                    value={provinsi}
                                    className={`form-control ${errors.provinsi ? 'is-invalid' : ''}`}
                                    onChange={e => setProvinsi(e.target.value)} />
                                {errors.provinsi && <div className="invalid-feedback"> {errors.provinsi} </div>}
                            </div>

                            <div className="form-group mb-2">
                                <label className="form-label">Tanggal Registrasi:</label>
                                <input
                                    type="date"
                                    placeholder="Tanggal Registrasi"
                                    name="tglRegistrasi"
                                    value={tglRegistrasi}
                                    className={`form-control ${errors.tglRegistrasi ? 'is-invalid' : ''}`}
                                    onChange={e => setTanggalRegistrasi(e.target.value)} />
                                {errors.tglRegistrasi && <div className="invalid-feedback"> {errors.tglRegistrasi} </div>}
                            </div>

                            <div className="form-group mb-2">
                                <select class="form-select" name="status" onChange={e => setStatus(e.target.value)}>
                                    <option >Select Status</option>
                                    <option value="aktif" selected={status === 'aktif'} >aktif</option>
                                    <option value="non-aktif" selected={status === 'non-aktif'}>non-aktif</option>
                                </select>
                            </div>

                            <button className="btn btn-success" onClick={saveOrUpdateCustomer}>Submit</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default CustomerComponent
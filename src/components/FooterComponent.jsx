const FooterComponent = () => {
    return (
        <div className="mt-4">
            <footer className='footer text-center'>
                <span>All right reserved 2023 by Mistra Minayasa</span>
            </footer>
        </div>
    )
}

export default FooterComponent
import HeaderComponent from "./components/HeaderComponent"
import './App.css'
import FooterComponent from "./components/FooterComponent"
import { BrowserRouter, Route, Routes } from "react-router-dom"
import ListCustomerComponent from "./components/customer/ListCustomerComponent"
import CustomerComponent from "./components/customer/CustomerComponent"

function App() {


  return (
    <>
      <BrowserRouter>
        <HeaderComponent />
        <Routes>
          <Route path="/" element={<ListCustomerComponent />} />
          <Route path="/customers" element={<ListCustomerComponent />} />
          <Route path="/add-customer" element={<CustomerComponent />} />
          <Route path="/edit-customers/:id" element={<CustomerComponent />} />
        </Routes>
        <FooterComponent />
      </BrowserRouter>
    </>
  )
}

export default App